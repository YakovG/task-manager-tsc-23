package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.service.IAuthService;
import ru.goloshchapov.tm.api.service.IUserService;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyLoginException;
import ru.goloshchapov.tm.exception.empty.EmptyPasswordException;
import ru.goloshchapov.tm.model.User;

import static ru.goloshchapov.tm.util.HashUtil.salt;
import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public final String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Nullable
    @Override
    public final User getUser() {
        final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    public final boolean isAuth() { return userId == null; }

    @Override
    @SneakyThrows
    public void checkRoles(final @Nullable Role... roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        @NotNull final Role role = user.getRole();
        for (@Nullable final Role item:roles) {
            if (item == null) continue;
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public void logout() { userId = null; }

    @Override
    @SneakyThrows
    public void login(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @Nullable final User user = userService.findUserByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.isLocked()) throw new AccessDeniedException();
        @Nullable final String hash = salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(@NotNull final String login,
                         @NotNull final String password,
                         @Nullable final String email
    ) {
        userService.create(login, password, email);
    }

    @Override
    public void registry(@NotNull final String login,
                         @NotNull final String password,
                         @Nullable final String email,
                         @NotNull final String role
    ) {
        userService.create(login, password, email, role);
    }

}
