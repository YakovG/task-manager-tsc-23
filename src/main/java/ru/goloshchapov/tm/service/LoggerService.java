package ru.goloshchapov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class LoggerService implements ILoggerService {

    @NotNull private static final String COMMANDS = "COMMANDS";
    @NotNull private static final String COMMANDS_FILE = "./access.log";

    @NotNull private static final String ERRORS = "ERRORS";
    @NotNull private static final String ERRORS_FILE = "./errors.log";

    @NotNull private static final String MESSAGES = "MESSAGES";
    @NotNull private static final String MESSAGES_FILE = "./messages.txt";

    @NotNull private final LogManager manager = LogManager.getLogManager();
    @NotNull private final Logger root = Logger.getLogger("");
    @NotNull private final Logger commands = Logger.getLogger(COMMANDS);
    @NotNull private final Logger errors = Logger.getLogger(ERRORS);
    @NotNull private final Logger messages = Logger.getLogger(MESSAGES);

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERRORS_FILE, true);
        registry(messages, MESSAGES_FILE, true);
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream("/logger.properties"));
        } catch (@NotNull final IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry (
            @NotNull final Logger logger, @NotNull final String filename, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(getConsoleHandler());
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(filename));
        } catch (@NotNull final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (isEmpty(message)) return;
        messages.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (isEmpty(message)) return;
        messages.fine(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (isEmpty(message)) return;
        commands.info(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

}
