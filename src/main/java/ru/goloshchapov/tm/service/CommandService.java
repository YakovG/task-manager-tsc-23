package ru.goloshchapov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.ICommandRepository;
import ru.goloshchapov.tm.api.service.ICommandService;
import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class CommandService implements ICommandService {

    @NotNull private final ICommandRepository commandRepository;

    @NotNull
    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Nullable
    @Override
    public Collection<AbstractCommand> getCommands() {return commandRepository.getCommands();}

    @Nullable
    @Override
    public Collection<AbstractCommand> getArguments() { return commandRepository.getArguments();}

    @Nullable
    @Override
    public Collection<String> getListCommandNames() {
        return commandRepository.getCommandNames();
    }

    @Nullable
    @Override
    public Collection<String> getListCommandArgs() {
        return commandRepository.getCommandArgs();
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (isEmpty(name)) return null;
        return commandRepository.getCommandByName(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        if (isEmpty(arg)) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
