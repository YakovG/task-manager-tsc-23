package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.IUserRepository;
import ru.goloshchapov.tm.api.service.IUserService;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.exception.auth.EmailExistsException;
import ru.goloshchapov.tm.exception.auth.LoginExistsException;
import ru.goloshchapov.tm.exception.empty.*;
import ru.goloshchapov.tm.exception.entity.UserByEmailNotFoundException;
import ru.goloshchapov.tm.exception.entity.UserByIdNotFoundException;
import ru.goloshchapov.tm.exception.entity.UserByLoginNotFoundException;
import ru.goloshchapov.tm.exception.incorrect.RoleIncorrectException;
import ru.goloshchapov.tm.model.User;

import static ru.goloshchapov.tm.util.HashUtil.salt;
import static ru.goloshchapov.tm.util.ValidationUtil.*;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    @SneakyThrows
    public final boolean isLoginExists(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        return userRepository.isLoginExists(login);
    }

    @Override
    @SneakyThrows
    public final boolean isEmailExists(@Nullable final String email) {
        if (isEmpty(email)) throw new EmptyEmailException();
        return userRepository.isEmailExists(email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isLoginExists(login)) throw new LoginExistsException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(salt(password));
        return userRepository.add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password, @Nullable final String email
    ) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyIdException();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password, @Nullable final Role role
    ) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final Role[] roles = Role.values();
        @NotNull final String roleChecking = role.toString();
        if (!checkInclude(roleChecking,toStringArray(roles))) throw new RoleIncorrectException(roleChecking);
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password,
            @Nullable final String email, @Nullable final String role
    ) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (isEmpty(role)) throw new EmptyRoleException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        @NotNull Role[] roles = Role.values();
        if (!checkInclude(role, toStringArray(roles))) throw new RoleIncorrectException(role);
        @NotNull final Role roleChecked = Role.valueOf(role);
        @NotNull final User user = create(login, password, email);
        user.setRole(roleChecked);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findUserByLogin(login);
        if (user == null) throw new UserByLoginNotFoundException(login);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findUserByEmail(@Nullable final String email) {
        if (isEmpty(email)) throw new EmptyEmailException();
        @Nullable final User user = userRepository.findUserByEmail(email);
        if (user == null) throw new UserByEmailNotFoundException(email);
        return user;
    }

    @Nullable
    @Override
    public User removeUser(@Nullable final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @Nullable final User user = userRepository.removeUserByLogin(login);
        if (user == null) throw new UserByLoginNotFoundException(login);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeUserByEmail(@Nullable final String email) {
        if (isEmpty(email)) throw new EmptyEmailException();
        @Nullable final User user = userRepository.removeUserByEmail(email);
        if (user == null) throw new UserByEmailNotFoundException(email);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @Nullable final User user = findOneById(userId);
        if (user == null) throw new UserByIdNotFoundException(userId);
        @Nullable final String hash = salt(password);
        user.setPasswordHash(hash);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @Nullable final User user = findOneById(userId);
        if (user == null) throw new UserByIdNotFoundException(userId);
        user.setFirstname(firstName);
        user.setLastname(lastName);
        user.setMiddlename(middleName);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (!isLoginExists(login)) throw new UserByLoginNotFoundException(login);
        return userRepository.lockUserByLogin(login);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (!isLoginExists(login)) throw new UserByLoginNotFoundException(login);
        return userRepository.unlockUserByLogin(login);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockUserByEmail(@Nullable final String email) {
        if (isEmpty(email)) throw new EmptyEmailException();
        if (!isEmailExists(email)) throw new UserByEmailNotFoundException(email);
        return userRepository.lockUserByEmail(email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockUserByEmail(@Nullable final String email) {
        if (isEmpty(email)) throw new EmptyEmailException();
        if (!isEmailExists(email)) throw new UserByEmailNotFoundException(email);
        return userRepository.unlockUserByEmail(email);
    }
}
