package ru.goloshchapov.tm.comparator;

import ru.goloshchapov.tm.entity.IHaveStatus;

import java.util.Comparator;

public final class ComparatorByStatus implements Comparator<IHaveStatus> {

    private static final ComparatorByStatus INSTANCE = new ComparatorByStatus();

    private ComparatorByStatus() {
    }

    public static ComparatorByStatus getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHaveStatus o1, final IHaveStatus o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getStatus().compareTo(o2.getStatus());
    }

}
