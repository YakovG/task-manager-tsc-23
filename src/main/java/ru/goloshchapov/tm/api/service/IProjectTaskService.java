package ru.goloshchapov.tm.api.service;

import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Project addProject(String userId, String projectName, String projectDescription);

    Task addTask(String userId, String taskName, String taskDescription);

    void clearProjects(String userId);

    List<Project> findAllProjects(String userId);

    List<Task> findAllByProjectId(String userId, String projectId);

    boolean isEmptyProjectById(String userId, String projectId);

    List<Task> findAllByProjectName(String userId, String projectName);

    boolean isEmptyProjectByName(String userId, String projectName);

    List<Task> findAllByProjectIndex(String userId, Integer projectIndex);

    boolean isEmptyProjectByIndex(String userId, Integer projectIndex);

    Task bindToProjectById(String userId, String taskId, String projectId);

    Task unbindFromProjectById(String userId, String taskId, String projectId);

    List<Task> removeAllByProjectId(String userId, String projectId);

    List<Task> removeAllByProjectName(String userId, String projectName);

    List<Task> removeAllByProjectIndex(String userId, Integer projectIndex);

    boolean removeAllByUserId(String userId);

    Project removeProjectById(String userId, String projectId);

    Project removeProjectByName(String userId, String projectName);

    Project removeProjectByIndex(String userId, Integer projectIndex);

    void createTestData();

}
