package ru.goloshchapov.tm.api.service;

import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.User;

public interface IAuthService {

    String getUserId();

    User getUser();

    boolean isAuth();

    void checkRoles(Role... roles);

    void logout();

    void login(String login, String password);

    void registry (String login, String password, String email);

    void registry (String login,
                   String password,
                   String email,
                   String role
    );

}
