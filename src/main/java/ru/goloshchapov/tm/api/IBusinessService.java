package ru.goloshchapov.tm.api;

import ru.goloshchapov.tm.model.AbstractBusinessEntity;

import java.util.List;

public interface IBusinessService<M extends AbstractBusinessEntity> extends IBusinessRepository<M> {

    List<M> sortedBy(String userId, String sortCheck);

    M updateOneById(String userId, String modelId, String name, String description);

    M updateOneByIndex(String userId, Integer index, String name, String description);

    M changeOneStatusById(String userId, String id, String statusChange);

    M changeOneStatusByName(String userId, String name, String statusChange);

    M changeOneStatusByIndex(String userId, int index, String statusChange);

}
