package ru.goloshchapov.tm.api;

import ru.goloshchapov.tm.model.AbstractBusinessEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<M extends AbstractBusinessEntity> extends IRepository<M>{

    void addAll(String userId, Collection<M> collection);

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    List<M> findAllByUserId(String userId);

    List<M> findAllStarted(String userId, Comparator<M> comparator);

    List<M> findAllCompleted(String userId, Comparator<M> comparator);

    M findOneById(String userId, String modelId);

    M findOneByIndex(String userId, Integer index);

    M findOneByName(String name);

    M findOneByName(String userId, String name);

    boolean isAbsentByName(String name);

    String getIdByName(String name);

    int size(String userId);

    void remove(String userId, M model);

    void clear(String userId);

    M removeOneById(String userId, String modelId);

    M removeOneByIndex(String userId, Integer index);

    M removeOneByName(String userId, String name);

    M startOneById(String userId, String modelId);

    M startOneByIndex(String userId, Integer index);

    M startOneByName(String userId, String name);

    M finishOneById(String userId, String modelId);

    M finishOneByIndex(String userId, Integer index);

    M finishOneByName(String userId, String name);

}
