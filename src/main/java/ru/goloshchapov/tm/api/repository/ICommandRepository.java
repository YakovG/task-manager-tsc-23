package ru.goloshchapov.tm.api.repository;

import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getArguments();

    Collection<AbstractCommand> getCommands();

    Collection<String> getCommandNames();

    Collection<String> getCommandArgs();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    void add(AbstractCommand command);
}
