package ru.goloshchapov.tm.api.repository;

import ru.goloshchapov.tm.api.IBusinessRepository;
import ru.goloshchapov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

    Task bindToProjectById (String userId, String taskId, String projectId);

    Task unbindFromProjectById(String userId, String taskId);

    List<Task> removeAllByProjectId(String userId, String projectId);

}
