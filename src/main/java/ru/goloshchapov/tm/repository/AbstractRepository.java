package ru.goloshchapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IRepository;
import ru.goloshchapov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull protected final List<E> list = new ArrayList<>();

    public Predicate<E> predicateById(final @NotNull String id) {
        return e->id.equals(e.getId());
    }

    @NotNull
    @Override
    public E add(@NotNull final E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null) return;
        list.addAll(collection);
    }

    @NotNull
    @Override
    public List<E> findAll() { return list; }

    @Nullable
    @Override
    public E findOneById(@NotNull final String id) {
        return list.stream()
                .filter(predicateById(id))
                .limit(1).findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findOneByIndex(@NotNull final Integer index) {return list.get(index);}

    @Override
    public boolean isAbsentById(@NotNull final String id) {
        return findOneById(id) == null;
    }

    @Override
    public boolean isAbsentByIndex(@NotNull final Integer index) {
        return findOneByIndex(index) == null;
    }

    @Nullable
    @Override
    public String getIdByIndex(@NotNull final Integer index) {
        @Nullable final E entity = findOneByIndex(index);
        if (entity == null) return null;
        return entity.getId();
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public void clear() { list.clear(); }

    @Override
    public void remove(@NotNull final E entity) { list.remove(entity); }

    @Nullable
    @Override
    public E removeOneById(@NotNull final String id) {
        final Optional<E> entity = Optional.ofNullable(findOneById(id));
        entity.ifPresent(list::remove);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeOneByIndex(@NotNull final Integer index) {
        final Optional<E> entity = Optional.ofNullable(findOneByIndex(index));
        entity.ifPresent(list::remove);
        return entity.orElse(null);
    }
}
