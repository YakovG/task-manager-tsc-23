package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.model.Task;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    public Predicate<Task> predicateByUserIdAndProjectId (
            @NotNull final String userId, @NotNull final String projectId
    ) {
        return t->userId.equals(t.getUserId()) && projectId.equals(t.getProjectId());
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @NotNull final String userId, @NotNull final String projectId
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return list.stream()
                .filter(predicateByUserIdAndProjectId(userId, projectId))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task bindToProjectById(
            @NotNull final String userId,
            @NotNull final String taskId,
            @NotNull final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final Optional<Task> task = Optional.ofNullable(findOneById(userId, taskId));
        task.ifPresent(t-> task.get().setProjectId(projectId));
        return task.orElse(null);

    }

    @Nullable
    @Override
    @SneakyThrows
    public Task unbindFromProjectById(
            @NotNull final String userId, @NotNull final String taskId
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final Optional<Task> task = Optional.ofNullable(findOneById(userId, taskId));
        task.ifPresent(t-> task.get().setProjectId(null));
        return task.orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> removeAllByProjectId(
            @NotNull final String userId, @NotNull final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final Optional<List<Task>> tasks = Optional.ofNullable(findAllByProjectId(userId, projectId));
        tasks.ifPresent(t-> tasks.get().forEach(this::remove));
        return tasks.orElse(null);
    }

}
