package ru.goloshchapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.ICommandRepository;
import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.*;
import java.util.stream.Collectors;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class CommandRepository implements ICommandRepository {

    @NotNull private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() { return arguments.values(); }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() { return commands.values(); }

    @NotNull
    @Override
    public Collection<String> getCommandNames() {
        @NotNull final List<String> result = new ArrayList<>();
        commands.values().stream().
                filter(c-> !isEmpty(c.name()))
                .collect(Collectors.toList())
                .forEach(c -> result.add(c.name()));
        return result;
    }

    @NotNull
    @Override
    public Collection<String> getCommandArgs() {
        @NotNull final List<String> result = new ArrayList<>();
        commands.values().stream().
                filter(c-> !isEmpty(c.arg()))
                .collect(Collectors.toList())
                .forEach(c -> result.add(c.arg()));
        return result;
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {return commands.get(name); }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@NotNull final String arg) {return arguments.get(arg);}

    @Override
    public void add(AbstractCommand command) {
        @Nullable final String arg = command.arg();
        @Nullable final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

}
