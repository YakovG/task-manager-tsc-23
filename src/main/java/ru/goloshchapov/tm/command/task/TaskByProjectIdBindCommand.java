package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByProjectIdBindCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-bind-to-project-by-id";

    @NotNull public static final String DESCRIPTION = "Add task to project by id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ADD TASK TO PROJECT");
        System.out.println("ENTER TASK ID");
        @Nullable final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getProjectTaskService().bindToProjectById(userId, projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
    }
}
