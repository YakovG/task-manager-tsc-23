package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskByProjectIndexRemoveAllCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-remove-all-by-project-index";

    @NotNull public static final String DESCRIPTION = "Remove all tasks by project index";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE ALL TASKS FROM PROJECT]");
        System.out.println("ENTER PROJECT INDEX");
        final int index = TerminalUtil.nextNumber() -1;
        @Nullable final List<Task> tasks = serviceLocator.getProjectTaskService().removeAllByProjectIndex(userId, index);
        if (tasks == null) throw new TaskNotFoundException();
        else tasks.clear();
    }
}
