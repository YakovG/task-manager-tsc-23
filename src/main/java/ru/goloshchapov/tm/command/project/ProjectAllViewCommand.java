package ru.goloshchapov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.Project;

import java.util.List;

public final class ProjectAllViewCommand extends AbstractCommand {

    @NotNull public static final String NAME = "project-view-all";

    @NotNull public static final String DESCRIPTION = "Show all projects";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ALL PROJECTS]");
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        int index = 1;
        for (@NotNull final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
