package ru.goloshchapov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class UserByLoginRemoveCommand extends AbstractCommand {

    @NotNull public static final String NAME = "user-remove-by-login";

    @NotNull public static final String DESCRIPTION = "Remove user by login";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE USER]");
        System.out.println("[ENTER LOGIN]");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final User user = serviceLocator.getUserService().removeUserByLogin(login);
        final boolean result = serviceLocator.getProjectTaskService().removeAllByUserId(user.getId());
        if (!result) System.out.println("User by login " + login + "has had an EMPTY project list");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
