package ru.goloshchapov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class UserByEmailUnlockCommand extends AbstractCommand {

    @NotNull public static final String NAME = "user-unlock-by-email";

    @NotNull public static final String DESCRIPTION = "Unlock user by email";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UNLOCK USER]");
        System.out.println("[ENTER EMAIL]");
        @Nullable final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByEmail(email);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
