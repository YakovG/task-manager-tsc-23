package ru.goloshchapov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @NotNull public static final String ARGUMENT = "-v";

    @NotNull public static final String NAME = "version";

    @NotNull public static final String DESCRIPTION = "Show application version";

    @NotNull
    @Override
    public String arg() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }
}
