package ru.goloshchapov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class HelpCommand extends AbstractCommand {

    @NotNull public static final String ARGUMENT = "-h";

    @NotNull public static final String NAME = "help";

    @NotNull public static final String DESCRIPTION = "Show terminal commands";

    @NotNull
    @Override
    public String arg() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (@NotNull final AbstractCommand command: commands) {
            @Nullable final String name = command.name();
            @Nullable final String arg = command.arg();
            @Nullable final String description = command.description();
            @NotNull String result = "";
            if (!isEmpty(name)) result += name;
            if (!isEmpty(arg)) result += " [" + arg + "] ";
            if (!isEmpty(description)) result += " - " + description;
            System.out.println(result);
        }
    }
}
